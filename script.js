// console.log("Hello World");

// JSON Object
	/*
		- JSON stands for JavaScript Object Notation
		- A common use of jason is to read ta from web server, and display the data in a webpage.
		- Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
	*/

	// JSON Object
		// {
		// 	"city": "Quezon City",
		// 	"province": "Metro Manila",
		// 	"country": "Philippines"
		// }

// JSON Array
	 // "cities":[
	 // 	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	 // 	{"city": "Pasig City", "province": "Metro Manila", "country": "Philippines"},
	 // 	{"city": "Navotas City", "province": "Metro Manila", "country": "Philippines"}
	 // ]

// JSON Methods
	
	/*

		JSON.stringify - converts JavaScript Object/Array into string.
		JSON.parse - converts JSON format to JavaScript Object

	*/
	
	let batchesArr = [
		{batchName: "Batch 177"},
		{batchName: "Batch 178"},
		{batchName: "Batch 179"}
	];

	// stringify
	console.log(batchesArr);
	let batchesArrCon = JSON.stringify(batchesArr);
	console.log(`Result from stringify method:`);
	console.log(batchesArrCon);

	let data = JSON.stringify({
		name: "John",
		age: 31,
		adress: {
			city: "Manila",
			country: "Philippines"
		}
	});

	let stringifiedObject = `{
		"name": "John",
		"age": 31,
		"adress":{
			"city": "Manila",
		"country": "Philippines"
	}
	}`;

	console.log(JSON.parse(stringifiedObject));

	console.log(data);

	// JSON.parse
	console.log(`Result from parse method:`);
	let dataParse = JSON.parse(data);
	console.log(dataParse);
	console.log(dataParse.name);

	console.log(JSON.parse(batchesArrCon));